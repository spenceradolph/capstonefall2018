<?php
session_abort();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Island Rush Homepage</title>
        <link rel="stylesheet" type="text/css" href="index.css">
    </head>

    <body>
        <h1>Island Rush Homepage</h1>

        <nav>
            <a class="active" href="index.php">Home</a>
            <a href="./login.php">Play the Game</a>
            <a href="adminLogin.php">Teacher Admin</a>
            <a href="ruleBook.php">Rule Book</a>
        </nav>

        <div class="spacer">
            <p>Welcome to Island Rush, the MSS251 Strategic Wargame created by Nic Marron, Bailey Little, JD Kalt, Griffin Gluck, and Juan Duque. Their board game has been computerized by Spencer Adolph, Jack Kulp, April Lewis, and Eric Yandura.</p>
        </div>
    </body>
</html>